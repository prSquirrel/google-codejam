package com.alexey.gcj

import io.Source
import java.io.PrintWriter


object B {
  def main(args: Array[String]): Unit = {
    val file = Source.fromFile(args(0))
    val lines = file.getLines
    val testCases = lines.next().toInt
    val pancakes: List[Vector[Boolean]] = lines.map {_.toVector.map {
        case '-' => false
        case '+' => true
      }.reverse
    }.toList
    file.close()

    val writer = new PrintWriter(args(1))

    var test = 1
    pancakes foreach { stack =>
      //println(s"Solving ${stack}")
      val res = solve(stack)
      writer.println(s"Case #${test}: $res")
      test += 1
    }

    writer.flush()
    writer.close()
  }

  // must accept reversed stack
  // to reduce complexity
  // of subsequent operations
  //
  // uses Vector for index splitting / last element access
  def solve(p: Vector[Boolean], flips: Int = 0): Int = {
    if(p.isEmpty) {
      flips
    }
    else {
      p.head match {
        // last pancake is not happy :(
        // lets break the day for his friends
        case false =>
          // +- and -+ are idempotent
          // with respect to dual function
          p match {
            // sequence of the form +- won't ever happen in this case,
            // so check only for -+ (we are operating in reverse!)
            case l if p.last == true =>
              // break the idempotency by
              // selecting last sequence
              val (left, right) = l.splitAt(l.lastIndexOf(false) + 1)
              // and flipping it
              val flipped = right.map(!_)
              solve(left ++ flipped, flips + 1)
            case l =>
              // flip all the pancakes
              // and remove the happy ones
              val flipped = dual(l).dropWhile(_ == true)
              solve(flipped, flips + 1)
          }
        case true =>
          val reduced = p.dropWhile(_ == true)
          solve(reduced, flips)
      }
    }
  }

  // flip pancakes
  def dual(p: Vector[Boolean]): Vector[Boolean] = {
    p.map(!_).reverse
  }
}
