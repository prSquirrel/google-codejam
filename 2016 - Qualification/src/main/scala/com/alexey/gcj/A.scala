package com.alexey.gcj

//import io.Source
import java.util.Scanner
import java.io.File
import java.io.PrintWriter

object A {
  def main(args: Array[String]): Unit = {
    val scanner = new Scanner(new File(args(0)))
    val writer = new PrintWriter(args(1))

    val testCases = scanner.nextInt()
    var test = 1
    while(scanner.hasNext) {
      val n = scanner.nextLong()
      val res = count(n).map(_.toString).getOrElse("INSOMNIA")
      writer.println(s"Case #${test}: $res")
      test += 1
    }
    writer.flush()
    writer.close()
  }

  def digitSet(number: Long) = {
    val digits = new Iterator[Long] {
      private[this] var n = number
      def hasNext = n > 0
      def next() = {
        val rem = n % 10
        n = n / 10
        rem
      }
    }
    if(number == 0) Set(0L) else digits.toSet
  }

  def sequence(number: Long) = {
    def loop(i: Int): Stream[Long] = {
      (i * number) #:: loop(i + 1)
    }
    loop(1)
  }

  def count(number: Long): Option[Long] = {
    def ss = sequence(number).scanLeft(Set.empty[Long]) { (s, x) =>
      s ++ digitSet(x)
    }
    if(number == 0) None else Some(ss.takeWhile(_.size < 10).size * number)
  }
}
