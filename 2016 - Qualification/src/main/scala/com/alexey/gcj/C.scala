package com.alexey.gcj

import java.util.Scanner
import java.io.File
import java.io.PrintWriter


object C {
  def main(args: Array[String]): Unit = {
    val scanner = new Scanner(new File(args(0)))
    val tests = scanner.nextInt() + 1

    val writer = new PrintWriter(args(1))

    var test = 1
    while(test < tests) {
      val bits = scanner.nextInt()
      val samples = scanner.nextInt()
      writer.println(s"Case #${test}:")
      //val it = NotPrimeBitInt.generateString(bits - 2)
      for(i <- 1 to samples) {
        println(s"Processing ${i}")
        val (jc, seal) = jamCoin(bits)//, it)
        writer.print(s"${jc}")
        seal.foreach { x =>
          writer.print(" ")
          writer.print(x)
        }
        writer.println()
      }
      test += 1
    }

    scanner.close()
    writer.flush()
    writer.close()
  }
  //val gs = NotPrimeBitInt.generateString(32 - 2)
  def jamCoin(bits: Int): (BigInt, Vector[BigInt]) = {
    if(bits < 4) throw new IllegalArgumentException("There are no jamcoins with length < 4.")

    val decimal = NotPrimeBitInt.next(bits)
    val radices = {
      val ss = decimal.toString()
      for(base <- 9 to 2 by -1) yield BigInt(ss, base)
    }
    val v: Vector[BigInt] = Vector(decimal) ++ radices
    val factors: Vector[BigInt] = v.map {
      new PrimeFactors(_).next
    }
    (v.head, factors)
  }
}

object NotPrimeBitInt {
  // def generateString(n: Int, pattern: String = ""): Iterator[String] = {
  //   if(n <= 0) Iterator('1' + pattern + '1')
  //   else generateString(n - 1, pattern + '0') ++ generateString(n - 1, pattern + '1')
  // }
  private[this] val rand = util.Random
  private[this] val repo = collection.mutable.Set.empty[BigInt]
  def next(bits: Int): BigInt = {
    //nextInt(max) is in range [0, max)
    val rands = List.fill[Int](bits - 2)(rand.nextInt(2))
    val str = '1' + rands.mkString + '1'
    val bi = BigInt(str)
    val maxRepoSize = Math.pow(2, bits - 2).toInt - 1
    if(repo.contains(bi)) next(bits)
    else {
      repo += bi
      bi
    }
  }
}


class PrimeFactors(n: BigInt) extends Iterator[BigInt] {
  val zero = BigInt(0)
  val one = BigInt(1)
  val two = BigInt(2)
  def isPrime(n: BigInt) = n.isProbablePrime(4)
  var currentN = n
  var prime = two

  def nextPrime = if (prime == two) {
      prime += one
    } else {
      prime += two
      while (!isPrime(prime)) {
        prime += two
        if (prime * prime > currentN)
          prime = currentN
      }
    }

  def next = {
    if (!hasNext)
      throw new NoSuchElementException("next on empty iterator")

    while(currentN % prime != zero) {
      nextPrime
    }
    currentN /= prime
    prime
  }

  def hasNext = currentN != one && currentN > zero
}
