name := """GCJ 2016 Qualification round"""

version := "0.0.1"

scalaVersion := "2.11.8"

libraryDependencies += "org.typelevel" %% "cats" % "0.4.1"

fork in run := true
cancelable in Global := true
javaOptions in run ++= Seq(
      "-Xmx2G",
      "-XX:+UseConcMarkSweepGC"
      //"-XX:+UseG1GC"
      //"-XX:MaxGCPauseMillis=15",
      //"-XX:+PrintGCDetails",
      //"-XX:+PrintGCTimeStamps",
      //"-verbose:gc"
    )
