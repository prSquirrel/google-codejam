package A

object A extends CodeJam with App {
  val filename = "A-large"

  run {
    val t = readInt//; readLine
    for(i <- 1 to t) {
      val parties = readInt
      val abc: Map[Char, Int] =
        (for(i <- 0 until parties) yield ((i+'A').toChar -> readInt))(collection.breakOut)
      val sol = solve(abc, List.empty[Char])
      // val gr = sol.grouped(2).grouped(2).foldLeft(List.empty[String]) {
      //   case (acc, a :: b :: c) =>
      //     if(a == b) a.mkString("") :: b.reverse.mkString("") :: acc
      //     else a.mkString("") :: b.mkString("") :: acc
      //   case (acc, a :: Nil) => a.mkString("") :: acc
      // }
      val gr = sol.grouped(2).grouped(2).foldLeft(List.empty[String]) {
        case (acc, x :: z :: Nil) =>
          if (x == z) x.reverse.mkString("") :: x.mkString("") :: acc
          else {
            if(z.tail.size == 1) {
              z.mkString("") :: x.mkString("") :: acc
            }
            else {
              x.mkString("") :: z.mkString("") :: acc
            }
          }
        case (acc, a :: Nil) =>
          if(a.size == 1) {
            acc.head :: a.mkString("") :: acc.tail
          }
          else {
            a.mkString("") :: acc
          }
      }.reverse

      //System.out.println(sol.grouped(2).grouped(2).mkString(","))

      printCase(gr.mkString(" "))
    }
  }

  def solve(politics: Map[Char, Int], evac: List[Char]): List[Char] = {
    if(!politics.isEmpty) {
      val (party, count) = politics.maxBy(_._2)
      if(politics(party) > 0) {
        solve(politics.updated(party, count - 1), party :: evac)
      } else {
        solve(politics - party, evac)
      }
    }
    else evac.reverse
  }
}

abstract class CodeJam {
  def filename: String

  private[this] lazy val scanner   = new java.util.Scanner(new java.io.FileInputStream(filename + ".in") )
  private[this] lazy val outStream = new java.io.BufferedOutputStream(new java.io.FileOutputStream(filename + ".out") )

  private[this] var curCase = 1
  def printCase(x: Any): Unit = { println(s"Case #${curCase}: $x"); curCase += 1 }

  def next      : String = scanner.next()
  def readInt   : Int    = scanner.nextInt()
  def readDouble: Double = scanner.nextDouble()
  def readLong  : Long   = scanner.nextLong()
  def readLine  : String = scanner.nextLine()

  def run(body: => Unit): Unit = {
    Console.withOut(outStream)(body)
    outStream.close()
    scanner.close()
  }
}
