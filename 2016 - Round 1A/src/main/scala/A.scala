package A

object A extends CodeJam with App {
  val filename = "A-large"
  run {
    val t = readInt; readLine
    for(i <- 1 to t) {
      printCase(solve(readLine))
    }
  }

  def solve(s: String) = {
    s.foldLeft("") { (acc, c) =>
      if(!acc.isEmpty && c >= acc.head) c + acc
      else acc + c
    }
  }
}

abstract class CodeJam {
  def filename: String

  private[this] lazy val scanner   = new java.util.Scanner(new java.io.FileInputStream(filename + ".in") )
  private[this] lazy val outStream = new java.io.BufferedOutputStream(new java.io.FileOutputStream(filename + ".out") )

  private[this] var curCase = 1
  def printCase(x: Any): Unit = { println(s"Case #${curCase}: $x"); curCase += 1 }

  def next      : String = scanner.next()
  def readInt   : Int    = scanner.nextInt()
  def readDouble: Double = scanner.nextDouble()
  def readLong  : Long   = scanner.nextLong()
  def readLine  : String = scanner.nextLine()

  def run(body: => Unit): Unit = {
    Console.withOut(outStream)(body)
    outStream.close()
    scanner.close()
  }
}
