package B

object B extends CodeJam with App {
  val filename = "B-large-practice"
  run {
    val cases = readInt
    for(i <- 0 until cases) {
      val N = readInt
      val xs = for {
        _ <- 0 until 2*N - 1
        x <- for(_ <- 0 until N) yield readInt
      } yield x

      val freqs = xs.foldLeft(Map.empty[Int, Int]) { (acc, x) =>
        if(acc contains x) acc + (x -> (acc(x) + 1))
        else acc + (x -> 1)
      }

      val odds = freqs.filter(_._2 % 2 != 0).keys.toList.sorted.mkString(" ")
      printCase(odds)
    }
  }
}


abstract class CodeJam {
  def filename: String

  private[this] lazy val scanner   = new java.util.Scanner(new java.io.FileInputStream(filename + ".in") )
  private[this] lazy val outStream = new java.io.BufferedOutputStream(new java.io.FileOutputStream(filename + ".out") )

  private[this] var curCase = 1
  def printCase(x: Any): Unit = { println(s"Case #${curCase}: $x"); curCase += 1 }

  def next      : String = scanner.next()
  def readInt   : Int    = scanner.nextInt()
  def readDouble: Double = scanner.nextDouble()
  def readLong  : Long   = scanner.nextLong()
  def readLine  : String = scanner.nextLine()

  def run(body: => Unit): Unit = {
    Console.withOut(outStream)(body)
    outStream.close()
    scanner.close()
  }
}
